#!/usr/bin/env sh
#################################################
# Authors
#   Coloc Malakof <colocmalakof@gmail.com>
# Documentation
#   https://gitlab.com/colocmalakof/flactwoogg
#################################################

# Exit immediately if a command exits with a non-zero exit status
set -e

# Treat unset variables as an error when substituting
set -u

# Constants
HOME_DIRECTORY="/flactwoogg"

CONFIG_DIRECTORY="${HOME_DIRECTORY}/config"

echo "Moving the flactwoogg' config file to ${CONFIG_DIRECTORY} if needed..."
if [[ ! -f "${CONFIG_DIRECTORY}/flactwoogg.yaml" ]]; then
    cp "/etc/flactwoogg/config.yaml" "${CONFIG_DIRECTORY}/flactwoogg.yaml"
fi

echo "Starting flactwoogg..."
flactwoogg --config="${CONFIG_DIRECTORY}/flactwoogg.yaml"
