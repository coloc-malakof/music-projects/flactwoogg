# `flactwoogg`

A quick and dirty multi-threaded Python script to convert `flac` files to `ogg` files.

## Operations

The following operations are available:

- Encode flac files\
  Decode `flac` files to `ogg` files using `oggenc` command.
- Add ReplayGain\
  Add ReplayGain using `vorbisgain` to `ogg` files.
- Tag ogg files\
  Tag `ogg` files using `vorbiscomment` command.
- Add comment tag\
  Add a comment tag to the `ogg` files using `vorbiscomment` command.
- Move cover art\
  Move the cover art from the source to the destination.
- New cover art name\
  Optionnaly rename the cover art from the source directory to the destination directory.
- Remove flac files\
  Allow to remove the `flac` files after processing.
- Remove ogg files\
  Allow to remove the `ogg` files after processing (for debug propose).
- Remove cover files from source\
  Allow to remove the cover files from source after processing.
- Remove cover files from destination\
  Allow to remove the cover files from destination after processing (for debug propose).
- Ignore if ogg directory exists\
  Ignore the processing if the destination directory exists.
- Rename path with encoding preset\
  Allow to rename the path of source directory to destination directory with the encoding preset used.
- Path pattern to replace\
  The path pattern to replace with the audio format used.

## Configuration file

How `flactwoogg` processes files are specified in a `.yaml` configuration file.

If no configuration file is specified from the command line argument `--config`, the default [`flactwoogg.yaml`](https://gitlab.com/colocmalakof/flactwoogg/blob/master/flactwoogg.yaml) in the same directory as the Python script `flactwoogg` will be used (if found).

## Usage

### Standalone

The following programs must be installed for `flactwoogg` to work correctly:

- `flac`
- `metaflac`
- `oggenc`
- `vorbisgain`
- `vorbiscomment`

```
flactwoogg [-h] [--config CONFIG_FILE]

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG_FILE  the YAML configuration file to use.
```

### `docker`

`docker` images are hosted on GitLab and can be pulled directly from there:

```
docker pull registry.gitlab.com/colocmalakof/flactwoogg:latest
```

Then the container can be ran mapping the different volumes on the host computer:

```
docker container run -v /tmp/flactwoogg/config:/config -v /tmp/flactwoogg/flac:/flac -v /tmp/flactwoogg/logs:/logs -v /tmp/flactwoogg/lyrics:/lyrics -v /tmp/flactwoogg/mp3:/mp3 -v /tmp/flactwoogg/wav:/wav registry.gitlab.com/colocmalakof/flactwoogg
```

## Technical details

### Encoding presets

One encoding presets are avaiable:

- '`Q5`' using the following arguments:\
  `oggenc --discard-comments --quality 5`
