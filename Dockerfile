#################################################
# Authors
#   Coloc Malakof <colocmalakof@gmail.com>
# Description
#   This is an optimized flactwoogg docker image.
# Documentation
#   https://gitlab.com/colocmalakof/flactwoogg
#################################################

FROM alpine:edge as vorbisgain

RUN set -xe && \
    # Install required packages
    apk add --no-cache \
        build-base \
        libogg-dev \
        libvorbis-dev \
        unzip
RUN wget "https://www.sjeng.org/ftp/vorbis/vorbisgain-0.36.zip" && \
    unzip vorbisgain-*.zip && \
    cd vorbisgain-* && \
    ./configure && \
    make && \
    mv vorbisgain ..

# Baseline
FROM alpine:edge

# Metadata
LABEL Maintainer="colocmalakof" \
    Version="1.0" \
    Description="Optimized flactwoogg docker image."

# Environment variables for the container
ENV UID=1000 \
    GID=1000

# Configure the system
RUN set -xe && \
    # Install required packages
    apk add --no-cache \
        flac \
        python3 \
        py3-eyed3 \
        py3-pip \
        vorbis-tools && \
    pip3 install --no-cache \
        ruamel.yaml && \
    # Create home directory
    mkdir /flactwoogg && \
    # Create symlinks
    ln -s /flactwoogg/config /config && \
    ln -s /flactwoogg/flac /flac && \
    ln -s /flactwoogg/logs /logs && \
    ln -s /flactwoogg/ogg /ogg

# Copy the files into the container
COPY build/flactwoogg.sh /usr/bin/flactwoogg.sh
COPY build/entrypoint.sh /usr/bin/entrypoint.sh
COPY flactwoogg.yaml /etc/flactwoogg/config.yaml
COPY flactwoogg /usr/bin/flactwoogg
COPY --from=vorbisgain /vorbisgain /usr/bin/vorbisgain

# Volumes
VOLUME [ "/config", "/flac", "/logs", "/ogg" ]

# Command to execute when the container is created
CMD [ "/usr/bin/entrypoint.sh" ]
